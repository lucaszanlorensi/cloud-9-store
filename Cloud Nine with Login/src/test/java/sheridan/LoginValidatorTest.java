package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	@Test
	public void testIsValidLoginLengthRegular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "ramses123" ) );
	}
	
	@Test
	public void testIsValidLoginLengthBoundaryIn( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "ramses" ) );
	}
	
	@Test
	public void testIsValidLoginLengthBoundaryOut( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "ramse" ) );
	}
	
	@Test
	public void testIsValidLoginLengthExceptional( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "r" ) );
	}
	
	@Test
	public void testIsValidLoginNumbersRegular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "ramses123" ) );
	}
	
	@Test
	public void testIsValidLoginNumbersBoundaryIn( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "r1amss" ) );
	}
	
	@Test
	public void testIsValidLoginNumbersBoundaryOut( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "1ramse" ) );
	}
	
	@Test
	public void testIsValidLoginNumbersExceptional( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "1234" ) );
	}
}
