package sheridan;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LoginValidator {

	public static boolean isValidLoginName( String loginName ) {
		Pattern pattern = Pattern.compile("[a-zA-Z]{1}[a-zA-Z0-9]{5,}", Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(loginName);
		boolean matchFound = matcher.find();
		if(matchFound) {
			return true;
	    }
		else
		{
			return false;
		}
	}
}
